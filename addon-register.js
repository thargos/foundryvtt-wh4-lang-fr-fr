
/************************************************************************************/
const _patch_eis = () => {
  if ( WFRP4E && WFRP4E.symptoms && WFRP4E.symptoms["delirium"] == undefined ) { 
    WFRP4E.symptoms["delirium"] = "Délire";

    WFRP4E.symptomDescriptions["delirium"] = "Votre sensibilité va et vient, avec des moments de clarté remplaçés subitement Your sensibility comes and goes, with moments of clarity replaced suddenly par des accès de délire, des hallucinations et de la terreur. Faites un <b>Test de Force Mentale Intermédiaire (+0)</b> chaque heure, et consultez la table <b><a class='table-click' data-table='delirium'>Délires</a></b> table.";
                                                                                                                                                                                                      // '<br>' creates a new line     
                                                                                                    // This is how you can roll from chat cards. Wrap 'd10' in <b><a class = 'chat-roll'>___</a></b>
    WFRP4E.symptomTreatment["delirium"] = "Certaines autorités traitent le délire comme un fièvre, avec les mêmes prescriptions. Les médicaments coutent quelques sous à quelques pistoles, en seulement 10% sont efficaces.<br><br>Avec des soins corrects, un test de <b>Guérison Intermédiaire (0)</b> arrêtes les hallucinations pour <b><a class = 'chat-roll'>1d10</a></b> heures.<br><br>Il est aussi courant de soigner les patients délirants avec des drogues tranquillisantes, comme la Fleur de Lune ou même de la Racine de Mandragore, pour garder les patients calmes pendant la crise, les envoyant dans un sommeil agité jusqu'à ce qu'il erécupèrent ou meurent.";

    WFRP4E.symptoms["swelling"] = "Gonflement";

    WFRP4E.symptomDescriptions["swelling"] = "Une partie du corps gonfle jusqu'à plusieurs fois sa taille normale, devenant rouge vif ou violette et devenant presque inutilisable. La partie du corps affectée correspond normalement à une plaie ou une morsure provoquant le gonflement, ou l'endroit où une maladie ou une infection est entrée dans le corps. <br> <br> <b> Tête </b>: Les yeux et la langue enflent, les joues deviennent livides, la mâchoire est toujours ouverte. Il est impossible de manger, mais des liquides légers peuvent être sirotés en petites quantités. Tous les tests nécessitant la parole sont plus difficiles de 3 niveaux. <br> <b> Bras </b>: le bras et la main gonflent, les articulations des épaules et du coude ne peuvent plus bouger et la main devient inutile. Pour la durée du gonflement, le bras compte comme Amputé. <br> <b> Corps </b>: Le corps entier gonfle jusqu'à ce que la victime ne puisse plus porter de vêtements. Tous les tests impliquant le mouvement deviennent difficiles de 3 niveaux. <br> <b> Jambe </b>: La jambe gonfle de manière grotesque, devenant aussi large que la partie la plus large de la cuisse sur toute sa longueur. Le pied est presque indiscernable. Pour la durée du gonflement, la jambe compte comme amputée (WFRP, page 180).";

    WFRP4E.symptomTreatment["swelling"] = "La plupart des traitements consistent à plonger la partie affectée, ou parfois tout le corps, dans un bain d'eau glacée pour réduire la chaleur qui accompagne les gonflements. Un <b> Test de Guérison Difficile (-20) étendu </b> nécessitant +3 DR réduit le renflement de <b> <a class ='chat-roll'> 2d10 </a> </b> heures. Chaque test dure une heure. Le patient se retrouve avec l'Etat Exténué +1 pour chaque test effectué au cours du processus. <br> <br> A la place, certains médecins saignent le patient avec une lame ou des sangsues. Un <b>Test de Guérison étendu </b> réussi nécessitant +4 SL et des Outils (médecin) réduit le renflement de (<a class ='chat-roll'> 1d10 </a> + Bonus d'Endurance du patient) heures. Chaque test a une difficulté de base <b> impossible (-50) </b> et dure une demi-heure.";

    WFRP4E.difficultyModifiers["futile"] = -40;

    WFRP4E.difficultyModifiers["impossible"] = -50;

    WFRP4E.difficultyLabels["futile"] = "Futile (-40)";

    WFRP4E.difficultyLabels["impossible"] = "Impossible (-50)";
    
    WFRP4E.loreEffect["tzeentch"] = "Les cibles des sorts de Tzeentch sont déchirées par la magie transformatrice du Chaos. Les cibles affectées par un sort du savoir de Tzeentch doivent réussir un test d'endurance Intermédiaire (+0) ou gagner +1 Point de Corruption. S'ils réussissent leur test, ils gagnent à la place +1 Point de Chance, qui peut être utilisé normalement. Tels sont les caprices de Tzeentch";
  }
}

/************************************************************************************/
const _fix_post_module_loading = () => {
  
  // Detect and patch as necessary
  if (WFRP4E && WFRP4E.talentBonuses && WFRP4E.talentBonuses["vivacité"] == undefined) { 
    console.log("Patching WFRP4E now ....");
    WFRP4E.weaponQualities["distract"] = "PROPERTY.Distract"; // Patch missing quality
    
    WFRP4E.talentBonuses = {
          "perspicace": "int",
          "affable": "fel",
          "tireur de précision": "bs",
          "très fort": "s",
          "vivacité": "i",
          "reflexes foudroyants": "ag",
          "imperturbable": "wp",
          "très résistant": "t",
          "doigts de fée": "dex",
          "guerrier né": "ws"
    }
    WFRP4E.speciesSkills = {
      "human": [
        "Soins aux animaux",
        "Charme",
        "Calme",
        "Evaluation",
        "Ragot",
        "Marchandage",
        "Langue (Bretonnien)",
        "Langue (Wastelander)",
        "Commandement",
        "Savoir (Reikland)",
        "Corps à corps (Base)",
        "Projectiles (Arc)"
      ],
      "dwarf": [
        "Résistance à l'alcool",
        "Calme",
        "Résistance",
        "Divertissement (Raconter)",
        "Evaluation",
        "Intimidation",
        "Langue (Khazalid)",
        "Savoir (Nains)",
        "Savoir (Geologie)",
        "Savoir (Metallurgie)",
        "Corps à corps (Base)",
        "Métier (Au choix)"
      ],
      "halfling": [
        "Charme",
        "Résistance à l'alcool",
        "Esquive",
        "Pari",
        "Marchandage",
        "Intuition",
        "Langue (Mootland)",
        "Savoir (Reikland)",
        "Perception",
        "Escamotage",
        "Discrétion (Au choix)",
        "Métier (Cuisinier)"
      ],
      "helf": [
        "Calme",
        "Divertissement (Chant)",
        "Evaluation",
        "Langue (Eltharin)",
        "Commandement",
        "Corps à corps (Base)",
        "Navigation",
        "Perception",
        "Musicien (Au choix)",
        "Projectiles (Arc)",
        "Voile",
        "Natation"
      ],
      "welf": [
        "Athlétisme",
        "Escalade",
        "Résistance",
        "Divertissement (Chant)",
        "Intimidation",
        "Langue (Eltharin)",
        "Corps à corps (Base)",
        "Survie en extérieur",
        "Perception",
        "Projectiles (Arc)",
        "Discrétion (Rural)",
        "Pistage"
      ],
    }
    WFRP4E.speciesTalents = {
      "human": [
        "Destinée",
        "Affable, Perspicace",
        3
      ],
      "dwarf": [
        "Résistance à la Magie",
        "Vision Nocturne",
        "Lire/Ecrire, Impitoyable",
        "Déterminé, Obstiné",
        "Costaud",
        0
      ],
      "halfling": [
        "Sens Aiguisé (Gout)",
        "Vision Nocturne",
        "Résistance (Chaos)",
        "Petit",
        0
      ],
      "helf": [
        "Sens Aiguisé (Vue)",
        "Imperturbable, Perspicace",
        "Vision Nocturne",
        "Seconde Vue, Sixième Sens",
        "Lire/Ecrire",
        0
      ],
      "welf": [
        "Sens Aiguisé (Sight)",
        "Dur à cuire, Seconde Vue",
        "Vision Nocturne",
        "Seconde Vue, Sixth Sense",
        "Lire/Ecrire",
        0
      ],
    }
    WFRP4E.species = {
      "human": "Humain",
      "dwarf": "Nain",
      "halfling": "Halfling",
      "helf": "Haut Elfe",
      "welf": "Elfe Sylvain"
    }
  } 
}

/************************************************************************************/
/* Manages /auberge command */
const _manage_inn_roll = async (content, msg) => {
    // Split input into arguments
    let command = content.split(" ").map(function(item) {
      return item.trim();
    })
    console.log(WFRP_Tables["talents"]);
    
    if (command[0] == "/auberge" && command[1] )
    {
      msg["type"] = 0;
      msg["rollMode"] = "gmroll";
      var compendium = game.packs.get('WH4-fr-translation.plats-dauberges');
      let rollList = [];
      await compendium.getIndex().then(index => rollList = index);
      //console.log("Got compendium...", rollList.length);
      for (var i=0; i< rollList.length; i++) {
        var rollTab = rollList[i];
        if ( rollTab.name.toLowerCase().includes(command[1].toLowerCase()) ) {
          let my_rollTable; 
          await compendium.getEntity(rollTab._id).then(mytab => my_rollTable = mytab);
          let myroll = my_rollTable.roll();
          //console.log("RES: ", myroll );
          msg.content = my_rollTable.name + " : " + myroll.results[0].text;
          //my_rollTable.draw();
          ChatMessage.create(msg);          
          return false;
        }
      }
    }
    if ( content.includes("/auberge") ) {
      msg["type"] = 0;
      msg["rollMode"] = "gmroll";
      msg["content"] = "Syntaxe : /auberge MOT_CLE, avec MOT_CLE parmi:<br>BoissonsBase, BoissonsFortes, Desserts, PlatsCommuns, PlatsExcellents, PlatsMaritimes, PlatsMédiocres, PlatsQualité,        PlatsRivières<br>Des raccourcis sont possibles avec une partie du nom : /auberge Base (correspond à BoissonBase) ou /auberge Mari (correspond à PlatsMaritimes), etc."      
      ChatMessage.create(msg);
      return false;
    }
}

/************************************************************************************/
let __eis_tables = { "animalmishap":1, "beasthead":1, "coincedentalenc":1, "demonic-mien":1, 
                     "expandedmutatemental":1, "expandedmutatephys":1, "fixations":1, 
                     "harmfulenc":1, "positiveenc":1, "weather":1, "mutatephys": 1
                    }
let __wfrp4e_tables =  { "career": 1, "critbody":1, "critleg": 1, "doom": 1, "eyes": 1, "majormis": 1, "mutatemental": 1, "oops": 1,  "species":1, "travel": 1,
                          "critarm": 1, "crithead": 1, "delirium": 1, "event": 1, "hair": 1, "minormis": 1, "mutatephys": 1, "talents": 1, "wrath": 1       
                       }
let __to_table_translate =  [ { name:"traits", transl:"Traits"}, {name:"talents", transl:"Talents"}, {name:"skills", transl:"Compétences"} , 
                              { name:"careers", transl:"Carrières"}, {name:"spells", transl:"Sorts"}, {name:"prayers", transl:"Bénédictions et Miracles" } ,
                              { name:"injuries", transl:"Blessures"}, {name:"criticals", transl:"Critiques"}, {name:"trappings", transl:"Equipement" },
                              { name:"bestiary", transl:"Bestiaire"}, { name:"diseases", transl:"Maladies"}
                            ]

/************************************************************************************/
const __create_translation_tables = async (compmod) => {
  for (let iterData of __to_table_translate) {
    
    let entityName = compmod+'.' + iterData.name;
    let compData  = game.packs.get( entityName);
    let compFull = await compData.getContent();
    let htmlTab = "<table border='1'><tbody>";
    for (let entryData of compFull ) {    
      htmlTab += "<tr><td>"+ entryData.data.originalName + "</td><td>@Compendium["+ entityName + '.' + entryData.id + "]{"+ entryData.name +"}</td></tr>\n";
    };  
    htmlTab += "</table>";
    let myjournal = await JournalEntry.create( {name: 'Traduction des ' + iterData.transl, content: htmlTab } );
    game.journal.insert( myjournal );
  }
}

/************************************************************************************
 * The default static compendium of translation tables must be aut-mapped to the relevant
 * compendium content name (ie either wfrp4e or wfrp4e-content up to now).
 *                                                                                     */
const __auto_patch_translation_journal_compendium = async (compmod) => {
  if (game.user.isGM) {
    let compData  = game.packs.get( "WH4-fr-translation.tables-des-traductions" );
    compData.locked = false;
    let translEntries = await compData.getContent();
    for (let entryData of translEntries ) {
      let mydata = duplicate(entryData.data);    
      mydata.content = mydata.content.replace(/__TO_REPLACE__/g, compmod );
      entryData.update( mydata );
    }
    compData.locked = true;
  }
}

/************************************************************************************/
const __check_fix_wrong_modules = ( chatFlag, patchFinished ) => {
  
  _fix_post_module_loading();
  
  game.modules.forEach((module, name) => {
    
    if ( name == "wfrp4e-content" && module.active) {
      FilePicker.browse("data", "modules/WH4-fr-translation/tables/").then(resp => {
        for (var file of resp.files) {
          let filename = file.substring(file.lastIndexOf("/")+1, file.indexOf(".json"));
          if ( __wfrp4e_tables[filename] == 1 ) { // Matching table name -> patch !
            fetch(file).then(r=>r.json()).then(records => {
              WFRP_Tables[filename] = records;
          });
        }
        }
      });
    }
    
    if ( name == "eis" && module.active) {
      _patch_eis();
      FilePicker.browse("data", "modules/WH4-fr-translation/tables/").then(resp => {
        for (var file of resp.files) {
          let filename = file.substring(file.lastIndexOf("/")+1, file.indexOf(".json"));
          if ( __eis_tables[filename] == 1 ) { // Matching table name -> patch !
            fetch(file).then(r=>r.json()).then(records => {
              WFRP_Tables[filename] = records;
          });
        }
        }
      });
      if (game.user.isGM && chatFlag)
        ChatMessage.create( { title: "Module EiS patché", content: "Le module EiS a été detecté et <strong>automatiquement patché</strong>.", whisper: ChatMessage.getWhisperRecipients("GM") } );      
    }
    
    if ( name == "wfrp4e-rnhd" && module.active ) {
      WFRP_Tables.career.name = "Carrières aléatoires";

      FilePicker.browse("data", "modules/WH4-fr-translation/tables/").then(resp => {
        for (var file of resp.files) {
          if ( file.match("career.json") ) {
            fetch(file).then(r=>r.json()).then(records => {
              let mycareer = records;
              for (let k=0; k<WFRP_Tables.career.rows.length; k++) {
                WFRP_Tables.career.rows[k].name = mycareer.rows[k].name; // Patch !!!
              }
          });
        }
        }
      });
      WFRP4E.speciesSkills["gnome"] =  [
          "Focalisation (Ulgu)",
          "Charme",
          "Résistance à l'alcool",
          "Esquive",
          "Divertissement (Au choix)",
          "Ragots",
          "Marchandage",
          "Langue (Ghassally)",
          "Langue (Magick)",
          "Langue (Wastelander)",
          "Survie en extérieur",
          "Discrétion (Au choix)"
        ];
      WFRP4E.speciesTalents["gnome"] = [
        "Insignifiant, Imprégné avec Uglu",
        "Chance, Imitation",
        "Vision Nocturne",
        "Pêcheur, Lire/Ecrire",
        "Seconde Vue, Sixième Sens",
        "Petit",
        0
      ];
      if (game.user.isGM && chatFlag)
        ChatMessage.create( { title: "Module RNHD patché", content: "<strong>Le module RHND a été detecté et automatiquement patché.</strong>", whisper: ChatMessage.getWhisperRecipients("GM") } );
    }
    if ( name == "wfrp4e-ew" && module.active && game.user.isGM && chatFlag) {    
      ChatMessage.create( { title: "Module non compatible detecté !", content: "<strong>Vous avez le module EW (wfrp4e-ew ?) installé. Malheureusement, ce module n'est pas compatible avec" +
      " les traductions et vient casser le fonctionnement de la traduction. Veuillez recopier les compendiums dans votre monde, désactiver le module et re-démarrer le monde</strong>",
        whisper: ChatMessage.getWhisperRecipients("GM")
      } );
    }
  });
  
  if (game.user.isGM && patchFinished)
    ChatMessage.create( { title: "Patch fini !", content: "Les modules WFRP4E ont été patchés <strong>avec succès</strong>. Vous pouvez y aller et que <strong>Shallya vous garde !</strong>", whisper: ChatMessage.getWhisperRecipients("GM") } );

}

/************************************************************************************/
const __add_actors_translation = ( ) => {
  const lang = game.settings.get('core', 'language');
  if ( lang == "fr" ) {    
    let pack_array = [];
    game.packs.forEach((pack, name) => {      
      let newpack = pack;
      if(!pack.translated && pack.entity === 'Actor') {        
        let translations = {
              "label": pack.metadata.name,
              "mapping": { 
                "name": "name",
                "description": "details.biography.value",              
                "items": {
                  "path": "items", 
                  "converter": "bestiary_traits"
                },
                "characteristics": {
                  "path": "data.characteristics",
                  "converter": "npc_characteristics"
                }
              },
              "entries": [
              ]
            };
        newpack = new TranslatedCompendium(pack, translations);
        console.log("Actor compendium has been replaced !!!!", pack.metadata.name);
      }
      //console.log("Parsing pack", newpack);
      if (newpack.metadata.name == "forien-armoury" ) {
        newpack.mapping.mapping["qualities"] = {
          "path": "data.qualities.value",
          "converter": "trapping_qualities_flaws"
          };
        newpack.mapping.mapping["flaws"] = {
          "path": "data.flaws.value",
          "converter": "trapping_qualities_flaws"
          };
        console.log("Forien armoury patched !!");
      }
      pack_array.push( [name, newpack ] );
    } );
    game.packs = new Collection( pack_array );
  }  
}

/************************************************************************************/
/* Hook for specific command */
Hooks.on("chatMessage", (html, content, msg) => {
  
  if ( content.includes("/auberge") ) {
    _manage_inn_roll( content, msg );
    return false;
  }
  
} );

/************************************************************************************/
/* Additionnal hooks ready */
Hooks.once('ready', () => {
    
  if (game.user.isGM)
    ChatMessage.create( { title: "Patch en progression", content: "Les modules WFRP4E sont <strong>en cours de patch pour traduction</strong>... Merci <strong>d'attendre le message de fin</strong> (dans environ 20 secondes)", whisper: ChatMessage.getWhisperRecipients("GM") } );      
  if ( isNewerVersion( "2.2.0", game.system.data.version )
    ChatMessage.create( { title: "Version trop récente", content: "<strong>Vous êtes en v3 du système Warhammer</strong>Ce module de traduction ne fonctionne plus avec ces versions. Allez sur le Discord FR pour plus d'informations.", whisper: ChatMessage.getWhisperRecipients("GM") } );

  setTimeout( __check_fix_wrong_modules, 2000, true, false);
  setTimeout( __check_fix_wrong_modules, 10000, false, false);
  setTimeout( __check_fix_wrong_modules, 20000, false, true);
  setTimeout( __add_actors_translation, 21000, false, true);
      
  let compmod = "wfrp4e";
  // Check various settings in the installation  
  game.modules.forEach((module, name) => {
    if ( name == "wfrp4e-content" && module.active) {
      compmod = "wfrp4e-content";
    }
  } );
  __auto_patch_translation_journal_compendium( compmod )
  /* Uncomment this to auto-create the translation tables 
     Auto-create translation journal tables
  __create_translation_tables(compmod);
  */
 //__create_translation_tables(compmod);
  
} );
